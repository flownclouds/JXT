<?php


function getfiles($path, $type)
{
    $list = [];
    foreach (scandir($path) as $afile) {

        if ($afile == '.' || $afile == '..') continue;
        $fullpath = $path . '/' . $afile;
        $isdir = is_dir($fullpath);
        switch ($type) {

            case "dir":
                if ($isdir) $list[] = $afile;
                break;
            case "file":
                if (!$isdir) $list[] = $afile;
                break;
            default:
                $list[] = $afile;
                break;
        }

    }
    return $list;
}
$path = $_SERVER['DOCUMENT_ROOT'] . '/tags';
$tags =$GLOBALS["tags"];// getfiles($path, 'dir');
foreach ($tags as $tag) {
    $_path = $path . '/' . $tag;
    $files = getfiles($_path, 'file');
    foreach ($files as $file) {
        $fullfilename = $_path . '/' . $file;
        if (str_replace(['/', '\\'], '', __FILE__) == str_replace(['/', '\\'], '', $fullfilename)) return;
        include_once($fullfilename);

    }

}


?>

