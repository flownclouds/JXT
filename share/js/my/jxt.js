//由id获取节点对象
jxt.getnode = function (id) {
    return nodeselect(jxt.data, id);
};
//创建tag新节点
jxt.newJson = function (tagname, attrs) {

    var node = jxt[tagname].dataJson;
    return jxt.route.exe(tagname + "." + "newJson", null, node, attrs); //jxt.route.getFunc(tagname+"."+"newJson")(node, attrs);
    /* var _tagname = jxtJSname(node, "newJson");
     return jxt[_tagname].newJson(node, attrs);*/
};

jxt.isLevOneNode = function (node) {
    switch ($.type(node)) {
        case "string":
            return jxt.isLevOneNode(jxt.getnode(node));
        default:
            return node.temp.pid == jxt.eleid;
    }

};
/*jxt.getLevOneEleID = function (id) {
    return getLevOneEle(id);
};*/
jxt.getJXTele = function (id) {
    var ele;
    if ($.type(id) == 'string') {
        if (!eleExits(id)) return null;
        ele = $("#" + id);
    } else ele = id;
    if (ele.attr("data-jxt-type")) return id;
    ele = $(ele).parent();
    if (ele) ele = ele[0];
    return jxt.getJXTele(ele);
};

//jxt.data 完全渲染
jxt.render = function (id, edit) {

    if (id) {
        var node = jxt.getnode(id);
        //jxt.route.getFunc(node.type+"."+"render")(node, edit);
        jxt.route.exe(node.type + "." + "render", null, node, edit);
        /*  var tagname = jxtJSname(node, "render");
         jxt[tagname].render(node, edit);*/

    } else {
        var data = jxt.data;
        var eleid = jxt.eleid;
        jxt.render(eleid);

    }

};

jxt.children = function (node) {
    if ($.type(node) == "string") node = jxt.getnode(node);

    return jxt.route.exe(node.type + "." + "children",null,node) ; //jxt.route.getFunc(node.type + "." + "children")(node);
    /* var tagname = jxtJSname(node, "children");
     return jxt[tagname].children(node);*/

};



jxt.init = function (type, attrs) {

    if (jxt.data.length == 0) {
        if (type == "pages") {
            var _pages = jxt.newJson("pages");
            jxt.data.push(_pages);
        } else {
            var _root = jxt.newJson("root", attrs);
            jxt.data.push(_root);
        }
    }


    jxt.dataInit();
    jxt.render();
    jxt.eve();


};

jxt.eve = function () {
    $("body").on("click", "*[data-jxt-eve]", function () {
        var eve = $(this).attr("data-jxt-eve");
        if (!eve)return;
        var id = $(this).attr("data-jxt-id");
        if (!id)return;
        var node = jxt.getnode(id);
        //jxt.route.getFunc(node.type + '.' + eve)(id);
        jxt.route.exe(node.type + '.' + eve,null,id);
    });
    $("body").on("click", "*[data-jxt-help]", function () {
        var eve = $(this).attr("data-jxt-help");
        if (!eve)return;
        var id = $(this).attr("data-jxt-id");
        if (!id)return;
        var node = jxt.getnode(id);
        jxt.msg.radio(id, "help_" + eve, {node: node});


    });
};

//全文初始化并渲染
jxt.reset = function () {
    jxt.dataInit();
    jxt.render();
};


jxt.addNode = function (newNode, ifInit) {
    switch ($.type(newNode)) {
        case "array":
            jxt.data = jxt.data.concat(newNode);
            break;
        default:
            jxt.data.push(newNode);
            break;
    }

    if (ifInit) jxt.dataInit();
};

jxt.delNode = function (id) {
    var ary = jxt.data[0].children;
    jxt.data[0].children.splice($.inArray(id, ary), 1);
    if (jxt.nowlevoneid == id) {
        jxt.nowlevoneid = null;
    }
};

//jxt.data 初始化
jxt.dataInit = function () {
    jxt.eleid = jxt.data[0].id;
    var node = jxt.data;
    var _obj_ = {};
    for (var i = 0; i < node.length; i++) {
        var nitem = node[i];
        var id = nitem.id;
        var _children = jxt.tag.children(nitem);
        if (_children) {
            for (var j = 0; j < _children.length; j++) {
                _obj_[_children[j]] = id;
            }
        }
        if (_obj_[id]) {
            if(!(node[i].temp))node[i].temp={};
            node[i].temp.pid = _obj_[id];
        }
    }
    jxt.data = node;
    //console.log(JSON.stringify(node));
    return jxt.data;
};



//保存json
jxt.saveJson = function () {
    var jsonobj = jxt.data;
    var str=JSON.stringify(jsonobj);
    console.log(str);
    localStorage.setItem(jsonobj[0].id, str);
};

//html渲染器
jxt.template = template;

jxt._addnode = function (subscribe_id, eventkey, attrs) {
    var newn = attrs.newnodelist;
    if ($.type(newn) == "array") {
        jxt.data = jxt.data.concat(newn);
    } else {
        jxt.data.push(newn);
    }
    console.log("_addnode");
    jxt._nodechange();
};

jxt._nodechange = function (subscribe_id, eventkey, attrs) {
    jxt.dataInit();
    console.log("_nodechange");
    jxt.saveJson();
};
jxt._editbegin = function (subscribe_id, eventkey, attrs, event_source_id) {
    if (jxt.isLevOneNode(event_source_id)) jxt.nowlevoneid = event_source_id;
};
jxt.msg.subscribe("jxt", "addmainnode", jxt._addnode);
jxt.msg.subscribe("jxt", "addchildnode", jxt._addnode);
jxt.msg.subscribe("jxt", "nodechange", jxt._nodechange);
jxt.msg.subscribe("jxt", "editbegin", jxt._editbegin);