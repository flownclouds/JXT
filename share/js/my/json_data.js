var HEdata;
HEdata = [
    {
        "id": "_jxtroot_",
        "type": "root",
        "title": "文章标题",
        "children": ['n43563', 'n43123', 'n431234', 'n432234']
    },
    {
        "id": "n43563",
        "type": "div",
        "children": ['n43363', 'n43323']
    },
    {
        "id": "n43363",
        "type": "text",
        "content": "我是一名程序员！"
    },
    {
        "id": "n43323",
        "type": "text",
        "content": "我的主页！",
        "url": "http://www.baidu.com"
    },
    {
        "id": "n43123",
        "type": "div",
        "children": ['n43193']
    },
    {
        "id": "n43193",
        "type": "img",
        "src": "https://ss0.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/logo_top_ca79a146.png",
        "url": "http://www.baidu.com"
    },
    {
        "id": "n431234",
        "type": "olist",
        "children": [['n431931', 'n431932'], ['n5454']]
    },
    {
        "id": "n431931",
        "type": "text",
        "content": "php"
    },
    {
        "id": "n431932",
        "type": "text",
        "content": "!!!!"
    },
    {
        "id": "n5454",
        "type": "text",
        "content": "mysql"
    },
    {
        "id": "n432234",
        "type": "table",
        "children": [[['n1', 'n2'], ['n3']], [['n4'], ['n5']]]
    },
    {
        "id": "n1",
        "type": "text",
        "content": "mysql1"
    },
    {
        "id": "n2",
        "type": "text",
        "content": "mysql2"
    },
    {
        "id": "n3",
        "type": "text",
        "content": "mysql3"
    },
    {
        "id": "n4",
        "type": "text",
        "content": "mysql4"
    },
    {
        "id": "n5",
        "type": "text",
        "content": "mysql5"
    }
];
/*

 var HEdata1 = {
 "title": "文章标题",
 "content": [
 {
 "type": "div",
 "content": [
 {
 "type": "text",
 "content": "我是一名程序员！"
 },
 {
 "type": "text",
 "content": "我的主页",
 "url": "http://www.baidu.com"

 }

 ]
 },
 {
 "type": "div",
 "content": [
 {
 "type": "img",
 "src": "https://ss0.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/logo_top_ca79a146.png",
 "url": "http://www.baidu.com"
 }

 ]
 },
 {
 "type": "div",
 "content": [
 {
 "type": "text",
 "content": "我的技能："
 }
 ]
 },
 {
 "type": "olist",
 "content": [[{"type": "text", "content": "php"}], [{"type": "text", "content": "js"}], [{
 "type": "text",
 "content": "mysql"
 }]]
 },
 {
 "type": "table",
 "content": [
 [[{"type": "text", "content": "php"}], [{"type": "text", "content": "js"}], [{
 "type": "text",
 "content": "mysql"
 }]],
 [[{"type": "text", "content": "php"}], [{"type": "text", "content": "js"}], [{
 "type": "text",
 "content": "mysql"
 }]]
 ]
 }
 ]
 };
 */
