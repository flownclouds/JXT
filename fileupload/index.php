<?php
/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

error_reporting(E_ALL | E_STRICT);
//require('UploadHandler.php');
//$upload_handler = new UploadHandler();

//echo  json_encode(["files"=>[["webpath"=>"/QQ20161101102004.png"]] ]);

//print_r($_FILES);
$f=$_FILES["uploaded_file"];

if (($f["size"] < 2000000)) {
    if ($f["error"]>0 ) {
        echo json_encode(["error" => true, "files" => [["webpath" => ""]]]);
    } else {
        //echo "Upload: " . $f["name"] . "<br />";
        // echo "Type: " . $f["type"] . "<br />";
        //echo "Size: " . ($f["size"] / 1024) . " Kb<br />";
        //echo "Temp file: " . $f["tmp_name"] . "<br />";
        $webpath="/upload/" . $f["name"];
        $filepath=$_SERVER["DOCUMENT_ROOT"].$webpath ;


        if (!file_exists($filepath)) {
            move_uploaded_file($f["tmp_name"],$filepath);
        
        }
        echo json_encode(["files" => [["webpath" => $webpath]]]);
    }
} else {
    echo json_encode(["error" => "size > 2000000", "files" => [["webpath" => ""]]]);
}
?>
